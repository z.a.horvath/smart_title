# About Smart Title

## Contents

 * [Introduction](#introduction)
 * [Requirements](#requirements)
 * [Installation](#installation)
 * [Configuration](#configuration)
 * [Maintainers](#maintainers)

## Introduction

Smart Title makes content entity labels visible and configurable on Field UI
forms.

The _Smart Title_ component could be enabled on [entity_type]and[bundle] level
and could switched on for each entity view mode.
For instance it can be used for _full_ view mode of the _Article_ content type
only.

_Entity label_ means e.g.:
 * Node title
 * Taxonomy term name
 * Commerce product name

## Requirements

The module does not define any hard dependencies.

## Installation

 * Install as you would normally install a contributed Drupal module.

## Configuration

Smart Title could be configured on Administration » Configuration »
Content authoring » Smart Title Settings.

## Maintainers

Current maintainers:

 * Zoltan A. Horvath (huzooka) - https://drupal.org/user/281301
